//#include GSNModel.h
#include ".\gsnmodel.h"



CGSNModel::CGSNModel(LPDIRECT3DDEVICE9 pd3dDevice)
{

	CvSize size;
	D3DVIEWPORT9 pViewport;
	//pViewport = new D3DVIEWPORT9;

    pd3dDevice->GetViewport( &pViewport );
    
	XMax = pViewport.Width;
	YMax = pViewport.Height;
	size.height = XMax;
	size.width	= YMax;
	dst_image = cvCreateImage( size, IPL_DEPTH_8U , 4 );
	color = cvCreateImage( size, IPL_DEPTH_8U , 3 );
    grey = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_8U, 1);
	edge = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_32F, 1);
	SIGSlezenie = false;		// int _scale	 = 100;
	SIGPusk = false;			// 

	rezVizirMax = 2.0f*Xmax/80.0f;			// ���������� ����� ����� ������ 

	xCntr = 0;				// ����� ����� ������
	yCntr = 0;

	// ���� ������ ��� - 1 ���
	// ���������� ��� �� ������� - +40..-40 ���
	// ������� �������� - 15 ���/���

	XOffset = 0;				// ���������� ����� ������ ������������ ������ ������������ �� ����� �����������
	YOffset = 0;
	_XOffset = 0;
	_YOffset = 0;

//	double xCntr;				//
//	double yCntr;
	xDelta = 0;				// �������� ���������� �� ������ ����� ���� ������ ������ ���
	yDelta = 0;

}

CGSNModel::~CGSNModel(void)
{
	cvReleaseImage(&color);
    cvReleaseImage(&edge);
    cvReleaseImage(&grey);

}

HRESULT CGSNModel::UpdateStatus( const float fElapsedTime )
{
    CvRect      r;
    CvMat       mat;

	// Calculate GSN tracing coordinates
/*	cvReleaseImage(&color);
    cvReleaseImage(&grey);

	color = cvCloneImage(src_image);
	cvSetImageROI( color, cvRect((320-35+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-35),70,70) );
    grey = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_8U, 1);
	edge = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_32F, 1);

	cvSetImageROI( grey, cvRect((320-35+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-35),70,70) );
	cvSetImageROI( edge, cvRect((320-35+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-35),70,70) );
*/
	cvSetImageROI( color, cvRect((XMax/2-rezVizirMax/2+(int)(XOffset)), (YMax/2-rezVizirMax/2+(int)(YOffset)),rezVizirMax,rezVizirMax) );
	cvSetImageROI( grey, cvRect((XMax/2-rezVizirMax/2+(int)(XOffset)), (YMax/2-rezVizirMax/2+(int)(YOffset)),rezVizirMax,rezVizirMax) );
	cvSetImageROI( edge, cvRect((XMax/2-rezVizirMax/2+(int)(XOffset)), (YMax/2-rezVizirMax/2+(int)(YOffset)),rezVizirMax,rezVizirMax) );

	cvCvtColor(color, grey, CV_RGB2GRAY);
	cvLaplace(grey,edge,3);
	cvConvertScaleAbs(edge,grey,2,0);
	cvThreshold(grey,grey,(int)(2.55f*GSNTrechhold),(int)(0.0f),CV_THRESH_TOZERO);
	cvResetImageROI(grey);
	cvMoments( cvGetSubRect(grey,&mat,cvRect((XMax/2-rezVizirMax*0.8/2+(int)(XOffset)), (YMax/2-rezVizirMax*0.8/2+(int)(YOffset)),rezVizirMax*0.8,rezVizirMax*0.8)), &moments, 0 );
    M00 = cvGetSpatialMoment( &moments, 0, 0 );
	if(M00 > 0 )
	{
		X = cvGetSpatialMoment( &moments, 1, 0 )/M00;
		Y = cvGetSpatialMoment( &moments, 0, 1 )/M00;
		xDelta = X-rezVizirMax*0.8/2 ;
		yDelta = Y-rezVizirMax*0.8/2 ;
		_xo = (int)((_XOffset));
		_yo = (int)((_YOffset));
		xo = (int)(XOffset);
		yo = (int)(YOffset);

		if(((_xo-10 < xo+xDelta || xo+xDelta < _xo+10 )||(_yo-10 < yo+yDelta || yo+yDelta < _yo+10 ))&&(scale > 0))
		{
			XOffset = XOffset + (int)(xDelta);
			YOffset = YOffset + (int)(yDelta);
			_XOffset = XOffset;
			_YOffset = YOffset;

			//cvSetTrackbarPos("XOffset", "image",_XOffset);
			//cvSetTrackbarPos("YOffset", "image",_YOffset);
		}
		else
		{
			XOffset = _XOffset;
			YOffset = _YOffset;
		}
		// Draw VIZIR 
	//	cvZero( dst_image );
	//	cvCopy(src_image,dst_image,NULL);
	//	cvCircle(dst_image, cvPoint(320+(int)(XOffset*3.2f),240+(int)(YOffset*2.4f)),20, CV_RGB(255,0,0),1,CV_AA ,0);
	//	cvShowImage( "image", dst_image );
	}
	else
	{
		X = rezVizirMax*0.8/2;
		Y = rezVizirMax*0.8/2;
		xDelta = 0;
		yDelta = 0;

		XOffset = _XOffset;
		YOffset = _YOffset;

		// Draw VIZIR 
//		cvZero( dst_image );
//		cvCopy(src_image,dst_image,NULL);
//		cvCircle(dst_image, cvPoint(320+(int)(XOffset*3.2f),240+(int)(YOffset*2.4f)),20, CV_RGB(255,0,0),1,CV_AA ,0);
//		cvShowImage( "image", dst_image );

	}
	xCntr = X;
	yCntr = Y;
//  (xc, yc) = (m10/m00,m01/m00)
//	cvSetImageROI( grey, cvRect((320-20+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-20),40,40) );
//	cvSetImageCOI(grey,0);
//	cvSetImageROI( color, cvRect((320-20+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-20),40,40) );
//	cvSetImageCOI(color,0);
//	cvCvtColor(grey,color,CV_GRAY2RGB);
//	cvCircle(color, cvPoint((int)(40*xCntr/40),(int)(40*yCntr/40)),5, CV_RGB(255,0,0),1,CV_AA ,0);
//    cvShowImage( "histogram", color );
	return S_OK;
}

void CGSNModel::SetFrameBuffer( DWORD* buffImg, DWORD size );
{
	cvReleaseImage(&color);
    cvReleaseImage(&grey);

	color = cvCreateImage(cvSize(XMax,YMax),IPL_DEPTH_8U,3);
	dst_image =cvCreateImage(cvSize(XMax,YMax),IPL_DEPTH_8U,4);
	cvSetData(dst_image, B, dst_image->width*4);
    cvCvtColor(dst_image, color, CV_RGBA2RGB);

//	color = cvCreateImage(cvSize(XMax,YMax),IPL_DEPTH_8U,4);


	//color = cvCloneImage(src_image);
    grey = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_8U, 1);
	edge = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_32F, 1);
	cvSetImageROI( color, cvRect((XMax/2-rezVizirMax+(int)(XOffset)), (YMax/2-rezVizirMax+(int)(YOffset)),rezVizirMax,rezVizirMax) );
	cvSetImageROI( grey, cvRect((XMax/2-rezVizirMax+(int)(XOffset)), (YMax/2-rezVizirMax+(int)(YOffset)),rezVizirMax,rezVizirMax) );
	cvSetImageROI( edge, cvRect((XMax/2-rezVizirMax+(int)(XOffset)), (YMax/2-rezVizirMax+(int)(YOffset)),rezVizirMax,rezVizirMax) );

}
