#pragma once
#ifdef _CH_
#pragma package <opencv>
#endif

#ifndef _EiC
#include "cv.h"
#endif

class CProfile;


struct strBuffImg
{
	DWORD *buff;
	DWORD num;
};

class CD3DGSN
{
	
public:
	LPDIRECT3DSURFACE9 m_pRenderSurfaceBack;
	LPDIRECT3DSURFACE9 m_pRenderSurface;
	LPDIRECT3DSURFACE9 m_pRenderSurfaceLck;
	int m_iBufferWidth;
    int m_iBufferHeight;
	float m_fKScale;
	strBuffImg* m_sBuffImg;
	DWORD m_dBuffNum ;
	DWORD m_dSizeBuff ; 
	D3DXVECTOR2 m_vCenter;
	bool m_bStarted;
	int hist_size;
	IplImage *src_image;
	IplImage	*color;
	IplImage	*edge;
	IplImage	*grey;
	CvMoments moments;
	double XOffset;
	double YOffset;
	double _XOffset;
	double _YOffset;

	double xCntr;
	double yCntr;
	double xCntrPrev;
	double yCntrPrev;

	double xDelta;
	double yDelta;
	double M00;
	double X;
	double Y;
	double XX;
	double YY;
	D3DXMATRIX m_mProj; //Camera projection matrix
	int iCropX;
	int iCropY;
	int iCropCenterX;
	int iCropCenterY;
	int iGolowaX;
	int iGolowaY;
	int iGolowaCenterX;
	int iGolowaCenterY;
	bool m_bTargetLocked;

public:
	CD3DGSN(void);
	~CD3DGSN(void);
    HRESULT InitDeviceObjects(int width, int height);
    HRESULT RestoreDeviceObjects(int width, int height);
    HRESULT FrameMove( const float fElapsedTime );
	HRESULT InvalidateDeviceObjects(void);
    HRESULT DeleteDeviceObjects();
    VOID    FinalCleanup();
	void GetDirectionRay(D3DXMATRIX* mView, D3DXVECTOR3* vDirOut,D3DXVECTOR3* vOriginOut,float* fAnglMesto,float* fAnglAzimut);
	void ResetGSN();
	void RenderBuffImage();
	void SaveBuffImage();
	bool GetTargetLocked(){ return m_bTargetLocked; };
	void SetTargetLocked( bool bLock ){ m_bTargetLocked = bLock; };
	
};
