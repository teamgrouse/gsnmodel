#pragma once
#ifdef _CH_
#pragma package <opencv>
#endif

#ifndef _EiC
#include "cv.h"
#include "highgui.h"
#include <stdio.h>
#include <D3DX9.h>
#include "D3DUtil.h"
#endif

class CGSNModel
{
protected:
	IplImage *dst_image = 0, *color = 0, *edge = 0,*grey = 0;

//	int _XOffset = 100;
//	int _YOffset = 100;

	double	GSNTrechhold;		// int _offset	 = 100; ���������������� �������
	bool	SIGSlezenie;		// int _scale	 = 100;
	bool	SIGPusk;			// 

	double XMax;				// ���������� ��������� ����� �� �����������
	double YMax;				// ���������� ��������� ����� �� ���������
	double rezVizirMax;			// ���������� ����� ����� ������ 

	double xCntr;				// ����� ����� ������
	double yCntr;

	// ���� ������ ��� - 1 ���
	// ���������� ��� �� ������� - +40..-40 ���
	// ������� �������� - 15 ���/���

	int XOffset;				// ���������� ����� ������ ������������ ������ ������������ �� ����� �����������
	int YOffset;

//	double xCntr;				//
//	double yCntr;
	double xDelta;				// �������� ���������� �� ������ ����� ���� ������ ������ ���
	double yDelta;
	
	CvRect      r;
    CvMat       mat;
	CvMoments moments;			//������� ������� ��������
	double M00,X,Y;
//	float offset;				//

public:
	CGSNModel(LPDIRECT3DDEVICE9 pd3dDevice);
	HRESULT UpdateStatus( const float fElapsedTime );
	void SetFrameBuffer( DWORD* buffImg );
	~CGSNModel(void);
};




