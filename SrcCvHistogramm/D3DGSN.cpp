#include "stdafx.h"
#include "d3dgsn.h"

CD3DGSN::CD3DGSN(void)
{
	m_pRenderSurfaceBack = NULL;
	m_pRenderSurface = NULL;
	m_pRenderSurfaceLck = NULL;
	m_iBufferWidth = 64;
    m_iBufferHeight = 64;
	m_fKScale = 1.0;
	m_sBuffImg = NULL;
	m_dBuffNum = 0;
	m_dSizeBuff = m_iBufferWidth*m_iBufferHeight;
	m_vCenter = D3DXVECTOR2((float)m_iBufferWidth/2.0f,(float)m_iBufferHeight/2);
	m_bStarted = false;
	D3DXMatrixIdentity(&m_mProj);
		
	hist_size = 64;
	src_image = NULL;
	color = NULL;
	edge = NULL;
	grey = NULL;
	XOffset = 0;
	YOffset = 0;
	_XOffset = 0;
	_YOffset = 0;

	xCntr = 0.0;
	yCntr = 0.0;
	xDelta = 0.0;
	
	yDelta = 0.0;
	M00 = 0.0;
	X = 0.0;
	Y = 0.0;
	XX = 0.0;
	YY = 0.0;
	
	iCropX = 0;
	iCropY = 0;
	iCropCenterX = 0;
	iCropCenterY = 0;
	iGolowaX  = 0;
	iGolowaY  = 0;
	iGolowaCenterX = 0;
	iGolowaCenterY = 0;

	m_bTargetLocked = false;
}

CD3DGSN::~CD3DGSN(void)
{
	FinalCleanup();
}


HRESULT CD3DGSN::InitDeviceObjects(int width, int height)
{
	bool createZBuffer = true;
	D3DFORMAT depthStencilFmt = D3DFMT_D16;

	m_iBufferWidth = width;
    m_iBufferHeight = height;
	m_dSizeBuff = m_iBufferWidth*m_iBufferHeight;
	m_dBuffNum = 2;
	m_vCenter = D3DXVECTOR2((float)m_iBufferWidth/2.0f,(float)m_iBufferHeight/2);
	
	m_sBuffImg = new strBuffImg[m_dBuffNum];
	for(DWORD i = 0; i < m_dBuffNum; i++)
	{
		m_sBuffImg[i].buff = new DWORD[m_dSizeBuff];
		m_sBuffImg[i].num = i;
	}
	g_pd3dDevice->GetRenderTarget(0, &m_pRenderSurfaceBack);

	hist_size = 64;
	src_image = NULL;
	color = NULL;
	edge = NULL;
	grey = NULL;
	XOffset = 0;
	YOffset = 0;
	xCntr= 0;
	yCntr= 0;
	xDelta= 0;
	yDelta= 0;
	M00= 0;
	X= 0;
	Y= 0;
	XX= 0;
	YY= 0;
	iCropX = iCropY = (int)(m_iBufferWidth*2.9f/D3DXToDegree(g_Profile.fFOVGSN));
	iCropCenterX = iCropCenterY = (int)(iCropX/2.0);

	iGolowaX = iGolowaY = (int)(m_iBufferWidth*2.5f/D3DXToDegree(g_Profile.fFOVGSN));
	iGolowaCenterX = iGolowaCenterY = (int)(iGolowaX/2.0);
	
	return S_OK;
}


HRESULT CD3DGSN::RestoreDeviceObjects(int width, int height)
{
	m_iBufferWidth = width;
    m_iBufferHeight = height;
	m_dSizeBuff = m_iBufferWidth*m_iBufferHeight;
	m_dBuffNum = 2;
	m_vCenter = D3DXVECTOR2((float)m_iBufferWidth/2.0f,(float)m_iBufferHeight/2);
	if(m_sBuffImg==NULL)
	{
		m_sBuffImg = new strBuffImg[m_dBuffNum];
		for(DWORD i = 0; i < m_dBuffNum; i++)
		{
			m_sBuffImg[i].buff = new DWORD[m_dSizeBuff];
			m_sBuffImg[i].num = i;
		}
	}

	FLOAT koeffExtension = 2.9f; //������� ��� ������ ������
	FLOAT koeffExtensionGolowa = 2.5f; //������� ��� ������ ������ ���� �������

	FLOAT fAspect = (FLOAT)m_iBufferWidth / (FLOAT)m_iBufferHeight;
	D3DXMatrixPerspectiveFovLH( &m_mProj, (g_Profile.fFOVGSN), fAspect, 0.010f, 12000.0f );
	iCropX = iCropY = (int)(m_iBufferWidth*koeffExtension/D3DXToDegree(g_Profile.fFOVGSN));//40.0f);
	iCropCenterX = iCropCenterY = (int)(iCropX/2.0);

	iGolowaX = iGolowaY = (int)(m_iBufferWidth*koeffExtensionGolowa/D3DXToDegree(g_Profile.fFOVGSN));
	iGolowaCenterX = iGolowaCenterY = (int)(iGolowaX/2.0);

		
	HRESULT hr = S_OK;
	if(m_pRenderSurface == NULL)
		hr = g_pd3dDevice->CreateRenderTarget( width, height, D3DFMT_A8R8G8B8, D3DMULTISAMPLE_NONE, 0, false,  &m_pRenderSurface, 0 );
	if(hr!=S_OK)
		return hr;
	if(m_pRenderSurfaceLck == NULL)
		hr = g_pd3dDevice->CreateOffscreenPlainSurface(width,height,D3DFMT_A8R8G8B8,D3DPOOL_SYSTEMMEM,&m_pRenderSurfaceLck,NULL);
	if(hr!=S_OK)
		return hr;
	if(m_pRenderSurfaceBack == NULL)
		g_pd3dDevice->GetRenderTarget(0, &m_pRenderSurfaceBack);

	if(src_image == 0)
	{
		src_image = cvCreateImage(cvSize(m_iBufferWidth, m_iBufferHeight), IPL_DEPTH_8U, 4);
		cvReleaseImageData(src_image);
		cvSetData(src_image, m_sBuffImg[0].buff, src_image->width*4);
	}


	return S_OK;
}

void CD3DGSN::ResetGSN()
{
	XOffset = 0;
	YOffset = 0;
	_XOffset = XOffset;
	_YOffset = YOffset;
	xCntr = XOffset;
	yCntr = YOffset;
	SetTargetLocked( false );
}

HRESULT CD3DGSN::FrameMove( const float fElapsedTime )
{
	int scale = 110;
    int offset = 45;
	double fColorScaleFactor = 2.5;
    float max_value = 0;
	int _xo, _yo, xo, yo;
 	if(src_image == 0)
		src_image = cvCreateImage(cvSize(m_iBufferWidth,m_iBufferHeight), IPL_DEPTH_8U, 4);
    CvMat mat;
	int xmax, ymax;
	xmax =((int)abs(XOffset) + iCropX);
	ymax =((int)abs(YOffset) + iCropY);
	// Calculate GSN tracing coordinates
	if(( xmax< m_vCenter.x)&&( ymax< m_vCenter.y))
	{
		cvReleaseImage(&color);
		cvReleaseImage(&grey);
		cvReleaseImage(&edge);

		color = cvCloneImage(src_image);
		cvSetImageROI( color, cvRect(((int)m_vCenter.x-iCropCenterX+(int)(XOffset)), ((int)m_vCenter.y+(int)(YOffset)-iCropCenterY), iCropX, iCropY) );
		grey = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_8U, 1);
		edge = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_32F, 1);
		cvSetImageROI( grey, cvRect(((int)m_vCenter.x-iCropCenterX+(int)(XOffset)), ((int)m_vCenter.y+(int)(YOffset)-iCropCenterY), iCropX, iCropY) );
		cvSetImageROI( edge, cvRect(((int)m_vCenter.x-iCropCenterX+(int)(XOffset)), ((int)m_vCenter.y+(int)(YOffset)-iCropCenterY), iCropX, iCropY) );

		cvCvtColor(color, grey, CV_RGBA2GRAY);
		cvLaplace(grey, edge, 3);
		cvConvertScaleAbs(edge, grey, fColorScaleFactor,0);
		cvThreshold(grey, grey, (int)(2.55f*offset), (int)(0.0f), CV_THRESH_TOZERO);
		cvResetImageROI(grey);
		if(((int)m_vCenter.x-iGolowaCenterX > (int)(XOffset) )&&((int)m_vCenter.y-iGolowaCenterY > (int)(YOffset) ))
		{
			cvMoments( cvGetSubRect(grey, &mat, cvRect(((int)m_vCenter.x-iGolowaCenterX+(int)(XOffset)), ((int)m_vCenter.y-iGolowaCenterY+(int)(YOffset)),iGolowaX,iGolowaY)), &moments, 0 );
			M00 = cvGetSpatialMoment( &moments, 0, 0 );
			if(M00 > 0 )
			{
				X = cvGetSpatialMoment( &moments, 1, 0 )/M00;
				Y = cvGetSpatialMoment( &moments, 0, 1 )/M00;
				xDelta = X - iGolowaCenterX ;
				yDelta = Y - iGolowaCenterY ;
				_xo = (int)((_XOffset));
				_yo = (int)((_YOffset));
				xo = (int)(XOffset);
				yo = (int)(YOffset);

				_XOffset = XOffset;
				_YOffset = YOffset;

				XOffset = XOffset + (xDelta);
				YOffset = YOffset + (yDelta);
				SetTargetLocked( true );
			}
			else
			{
				X = iGolowaCenterX;
				Y = iGolowaCenterY;
				xDelta = 0;
				yDelta = 0;
				_XOffset = XOffset;
				_YOffset = YOffset;
				XOffset = 0;
				YOffset = 0;
				SetTargetLocked( false );

			}
		}
	}
	else
	{
			XOffset = 0;
			YOffset = 0;
			_XOffset = XOffset;
			_YOffset = YOffset;
			SetTargetLocked( false );
	}
	xCntrPrev = xCntr;
	yCntrPrev = yCntr;

	double xVel = (XOffset-xCntrPrev)/fElapsedTime;
	double yVel = (YOffset-yCntrPrev)/fElapsedTime;
	double dSpeed = 200.0; //���������� ��� ������� ������� ���������,��� �������� ������ � ������� � ����

	if(xVel < dSpeed)
		xCntr = XOffset;
	else
		xCntr += (abs(xVel)/xVel)*dSpeed*fElapsedTime;

	if(yVel < dSpeed)
		yCntr = YOffset;
	else
		yCntr += (abs(yVel)/yVel)*dSpeed*fElapsedTime;

	m_bStarted = false;
	return S_OK;
}

HRESULT CD3DGSN::InvalidateDeviceObjects(void)
{
	SAFE_RELEASE(m_pRenderSurface);
	SAFE_RELEASE(m_pRenderSurfaceLck);
	SAFE_RELEASE(m_pRenderSurfaceBack);
	if(m_sBuffImg)
	{
		for(DWORD i =0; i< m_dBuffNum; i++)
		{
			if(m_sBuffImg[i].buff)
				delete [] m_sBuffImg[i].buff;
			m_sBuffImg[i].num = i;
		}
		delete [] m_sBuffImg;
		m_sBuffImg = NULL;
	}
	cvReleaseImageHeader(&src_image);
    cvReleaseImage(&color);
    cvReleaseImage(&edge);
    cvReleaseImage(&grey);
	return S_OK;
}

HRESULT CD3DGSN::DeleteDeviceObjects()
{
	SAFE_RELEASE(m_pRenderSurface);
	SAFE_RELEASE(m_pRenderSurfaceLck);
	SAFE_RELEASE(m_pRenderSurfaceBack);
	return S_OK;
}

VOID    CD3DGSN::FinalCleanup()
{
	SAFE_RELEASE(m_pRenderSurface);
	SAFE_RELEASE(m_pRenderSurfaceLck);
	SAFE_RELEASE(m_pRenderSurfaceBack);// = NULL;
	if(!(src_image == 0)) cvReleaseImageHeader(&src_image);

	if(m_sBuffImg)
	{
		for(DWORD i =0; i< m_dBuffNum; i++)
		{
			if(m_sBuffImg[i].buff)
				delete [] m_sBuffImg[i].buff;
			m_sBuffImg[i].num = i;
		}
		delete [] m_sBuffImg;
		m_sBuffImg = NULL;
	}
    cvReleaseImage(&color);
    cvReleaseImage(&edge);
    cvReleaseImage(&grey);
}


void CD3DGSN::RenderBuffImage()
{
    D3DLOCKED_RECT  d3dlr;
	DWORD* pDestPixel;
	g_pd3dDevice->GetRenderTargetData(m_pRenderSurface,m_pRenderSurfaceLck);
	if ((m_pRenderSurfaceLck->LockRect(&d3dlr, NULL,  D3DLOCK_READONLY  )))
	{		
		return;
	}
	pDestPixel = (DWORD*)d3dlr.pBits;
	for(DWORD i = 0; i<m_dSizeBuff;i++)
	{
		m_sBuffImg[0].buff[i] = pDestPixel[i];
	}
	m_sBuffImg[0].num = 0;
	m_pRenderSurfaceLck->UnlockRect();
	m_bStarted =true;
}

void CD3DGSN::SaveBuffImage()
{
	char strname[256];
	int numCldFile = 0;
	DWORD inh =0;
	int mipLevels = 1;
	DWORD usage = D3DUSAGE_RENDERTARGET;
	D3DFORMAT fmt = D3DFMT_A8R8G8B8;
	D3DPOOL pool = D3DPOOL_DEFAULT;
	LPDIRECT3DTEXTURE9 pTextSave;
	HRESULT hr;

	hr = D3DXCreateTexture(g_pd3dDevice, m_iBufferWidth, m_iBufferHeight, mipLevels, 
				   D3DUSAGE_DYNAMIC, fmt, D3DPOOL_SYSTEMMEM, &pTextSave);
	for(inh = 0; inh < m_dBuffNum; inh++)
	{
		sprintf(strname, "Z:\\testN%d.bmp", m_sBuffImg[inh].num);
		D3DLOCKED_RECT  d3dlr;

		if (FAILED(pTextSave->LockRect( 0, &d3dlr, NULL, D3DLOCK_DISCARD )))
			return ;
		DWORD* pDestPixel = (DWORD*)d3dlr.pBits;

		memcpy(pDestPixel, m_sBuffImg[inh].buff,(sizeof(DWORD)*m_dSizeBuff));
		pTextSave->UnlockRect(0);
		D3DXSaveTextureToFile( strname, D3DXIFF_BMP , pTextSave, NULL );
	}
	SAFE_RELEASE( pTextSave );
}

void CD3DGSN::GetDirectionRay(D3DXMATRIX* mView, D3DXVECTOR3* vDirOut, D3DXVECTOR3* vOriginOut, float* fAnglMesto, float* fAnglAzimut)
{
	D3DXMATRIX mInvView;
	D3DXVECTOR3 vLastT;
	D3DXVECTOR4 vPoint;
	
	vPoint.x = (float)(((xCntr+m_iBufferWidth*0.5f)*2.0f/m_iBufferWidth)-1.0f)/m_mProj._11;
	vPoint.y = (float)((1.0f - (yCntr+m_iBufferHeight*0.5f)*2.0f/m_iBufferHeight))/m_mProj._22;
	vPoint.z = 1.0f;

	(*fAnglMesto) = asin(vPoint.x);
	(*fAnglAzimut) = asin(vPoint.y);

	D3DXMatrixInverse(&mInvView, NULL, mView);

	vLastT.x = vPoint.x*mInvView._11+vPoint.y*mInvView._21+vPoint.z*mInvView._31;
	vLastT.y = vPoint.x*mInvView._12+vPoint.y*mInvView._22+vPoint.z*mInvView._32;
	vLastT.z = vPoint.x*mInvView._13+vPoint.y*mInvView._23+vPoint.z*mInvView._33;

	(*vOriginOut).x =mInvView._41;
	(*vOriginOut).y =mInvView._42;
	(*vOriginOut).z =mInvView._43;

	(*vDirOut).x =  vLastT.x;
	(*vDirOut).y =  vLastT.y;
	(*vDirOut).z =  vLastT.z;
}