#ifdef _CH_
#pragma package <opencv>
#endif

#ifndef _EiC
#include "cv.h"
#include "highgui.h"
#include <stdio.h>
//using namespace std;

#include <stdlib.h>
#include <stdarg.h>
#include <D3DX9.h>
//#include <new>

//#include <D3DX9.h>
//#include "DXUtil.h"
//#include <D3DX9.h>
#include "d3d9types.h"
#endif

char file_name[] = "GSNExample\\testN490.bmp";
char file_name2[] = "GSNExample\\testN491.bmp";

int _XOffset = 100;
int _YOffset = 100;
int _scale	 = 100;
int _offset	 = 100;

int hist_size = 64;
float range_0[]={0,256};
float* ranges[] = { range_0 };
IplImage *src_image = 0, *dst_image = 0, *color = 0, *edge = 0,*grey = 0;
CvHistogram *hist;
uchar lut[256];
CvMat* lut_mat;
CvMoments moments;

int XOffset;
int YOffset;

double xCntr = 0;
double yCntr = 0;
double xDelta;
double yDelta;
double M00,X,Y,XX,YY;


/* brightness/contrast callback function */
void update_brightcont( int arg )
{
    int scale = _scale - 100;
    int offset = _offset - 100;

    int i, bin_w;
    float max_value = 0;
	int _xo,_yo,xo,yo;
//    CvMoments   m;
    CvRect      r;
    CvMat       mat;
	// Calculate GSN tracing coordinates
	cvReleaseImage(&color);
    cvReleaseImage(&grey);
	cvReleaseImage(&edge);
    color = cvCloneImage(src_image);
	cvSetImageROI( color, cvRect((320-35+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-35),70,70) );
    grey = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_8U, 1);
	edge = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_32F, 1);
	cvSetImageROI( grey, cvRect((320-35+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-35),70,70) );
	cvSetImageROI( edge, cvRect((320-35+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-35),70,70) );
	
//  cvMorphologyEx(hist_image, hist_image, NULL, NULL, CV_MOP_OPEN, 8); 
//	cvAbsDiff(hist_image,histSrc_image,hist_image);
//	cvConvertScaleAbs(hist_image,hist_image,scale,0);
    cvCvtColor(color, grey, CV_RGBA2GRAY);
	cvLaplace(grey,edge,3);
	cvConvertScaleAbs(edge,grey,2,0);
	cvThreshold(grey,grey,(int)(2.55f*offset),(int)(0.0f),CV_THRESH_TOZERO);
	cvResetImageROI(grey);

	cvMoments( cvGetSubRect(grey,&mat,cvRect((320-20+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-20),40,40)), &moments, 0 );
    M00 = cvGetSpatialMoment( &moments, 0, 0 );
	if(M00 > 0 )
	{
		X = cvGetSpatialMoment( &moments, 1, 0 )/M00;
		Y = cvGetSpatialMoment( &moments, 0, 1 )/M00;
		xDelta = X-20 ;
		yDelta = Y-20 ;
		_xo = (int)((_XOffset-100)*3.2f);
		_yo = (int)((_YOffset-100)*2.4f);
		xo = (int)(XOffset*3.2f);
		yo = (int)(YOffset*2.4f);

		if(((_xo-10 < xo+xDelta || xo+xDelta < _xo+10 )||(_yo-10 < yo+yDelta || yo+yDelta < _yo+10 ))&&(scale > 0))
		{
			XOffset = XOffset + (int)(xDelta /3.2f);
			YOffset = YOffset + (int)(yDelta /2.4f);
			_XOffset = XOffset+100;
			_YOffset = YOffset+100;

			cvSetTrackbarPos("XOffset", "image",_XOffset);
			cvSetTrackbarPos("YOffset", "image",_YOffset);

		}
		else
		{
			XOffset = _XOffset - 100;
			YOffset = _YOffset - 100;

		}
		// Draw VIZIR 
		cvZero( dst_image );
		cvCopy(src_image,dst_image,NULL);
		cvCircle(dst_image, cvPoint(320+(int)(XOffset*3.2f),240+(int)(YOffset*2.4f)),20, CV_RGB(255,0,0),1,CV_AA ,0);
		cvShowImage( "image", dst_image );

	}
	else
	{
		X = 20;
		Y = 20;
		xDelta = 0;
		yDelta = 0;

		XOffset = _XOffset - 100;
		YOffset = _YOffset - 100;
	//	_XOffset =(int)(xDelta /3.2f);
	//	_YOffset =(int)(yDelta /2.4f);

		// Draw VIZIR 
		cvZero( dst_image );
		cvCopy(src_image,dst_image,NULL);
		cvCircle(dst_image, cvPoint(320+(int)(XOffset*3.2f),240+(int)(YOffset*2.4f)),20, cvScalar(255,0,0,255),1,CV_AA ,0);
		cvShowImage( "image", dst_image );

	}

	xCntr = X;
	yCntr = Y;
//  (xc, yc) = (m10/m00,m01/m00)
/*	cvResetImageROI(edge);
	cvSetImageCOI(edge,1);

	color = cvCreateImage(cvSize(color->width,color->height), IPL_DEPTH_32F, 3);
	cvResetImageROI(color);
	cvSetImageCOI(color,0);
	cvMerge(edge,edge,edge,NULL,color);
*/
	cvSetImageROI( grey, cvRect((320-20+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-20),40,40) );
	cvSetImageCOI(grey,0);
	cvSetImageROI( color, cvRect((320-20+(int)(XOffset*3.2f)), (240+(int)(YOffset*2.4f)-20),40,40) );
	cvSetImageCOI(color,0);
//	cvCvtColor(hist_image,hist_image,CV_HLS2RGB);
	cvCvtColor(grey,color,CV_GRAY2RGB);
	cvCircle(color, cvPoint((int)(40*xCntr/40),(int)(40*yCntr/40)),5, CV_RGB(255,0,0),1,CV_AA ,0);
    cvShowImage( "histogram", color );
}


int main( int argc, char** argv )
{
    CvScalar white = cvRealScalar(255);
    CvScalar black = cvRealScalar(0);
	int numCldFile = 495;
	double num = 495.0f;

	int NX = 640;
	int NY = 480;
	int R = NX*NY;

	DWORD *B = new DWORD[R];

	float X,Y,Dist;
	float Incry = 2.0f/NY;
	float Incrx = 2.0f/NX;
	int x = 0, y = 0;
	float value;
	Y = -1.0f;
	for (y = 0; y < NY; y++)
	{
		X = -1.0f;
		for (x=0; x<NX; x++)
		{
			Dist = (float)sqrt(X*X+Y*Y);
			if (Dist > 1) Dist=1;
			//our magical interpolation polynomical
			value = 2*Dist*Dist*Dist - 3*Dist*Dist + 1;
			value *= 0.5f;
			B[y*NX+x] = D3DCOLOR_RGBA((unsigned int)(1),(unsigned int)(2),(unsigned int)(3),(unsigned int)(4));//(unsigned char)(value * 255);
			X+=Incrx;
		}
		Y+=Incry;
	}


	sprintf(file_name, "GSNExample\\testN%d.bmp", numCldFile);

	// Load the source image. HighGUI use.
 //  src_image = cvLoadImage( argc == 2 ? argv[1] : file_name, 4 );
	src_image = cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,3);
	dst_image = cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,3);
    CvMat* mat = cvCreateMat( 480, 640, CV_8UC4 );
	CvMat* ch1 = cvCreateMat( 480, 640, CV_8UC1 );
	CvMat* ch2 = cvCreateMat( 480, 640, CV_8UC1 );
	CvMat* ch3 = cvCreateMat( 480, 640, CV_8UC1 );
	CvMat* ch4 = cvCreateMat( 480, 640, CV_8UC1 );
	
 //   cvCreateData( src_image );
	char* test;

//	test  = src_image->imageData;
//	delete test;
	cvReleaseImageData(src_image);
	cvSetImageData(src_image, B, src_image->width*4);
//	cvReleaseImageData(src_image);
//	src_image->imageData = test;
//	cvReleaseImageHeader(&src_image);
//	delete [] B;
 //  cvCvtColor(src_image, dst_image, CV_RGBA2RGB);

 //   cvGetImage(mat,src_image);

//	cvSplit(mat,ch1,ch2,ch3,ch4);
//	cvMerge(ch1,ch2,ch3,0,src_image);

/*   
  //  CvMat* rgba = cvCreateMat( 100, 100, CV_8UC4 );
    CvMat* rgb = cvCreateMat( mat->rows, mat->cols, CV_8UC3 );
    CvMat* alpha = cvCreateMat( mat->rows, mat->cols, CV_8UC1 );
    CvArr* out[] = { rgb, alpha };
    int from_to[] = { 0, 0, 1, 1, 2, 2, 3, 3 };
   // cvSet( rgba, cvScalar(1,2,3,4) );
    cvMixChannels( (const CvArr**)&mat, 1, out, 2, from_to, 4 );
	 cvGetImage(out[0],src_image);
*/

    if( !src_image )
    {
        printf("Image was not loaded.\n");
        return -1;
    }

    dst_image = cvCloneImage(src_image);
	xDelta = 0;
	yDelta = 0;

//	cvFillImage( circle_image, 0);
	
    color = cvCloneImage(src_image);//cvCreateImage(cvSize(40,40), 8, 1);

	hist = cvCreateHist(1, &hist_size, CV_HIST_ARRAY, ranges, 1);
    lut_mat = cvCreateMatHeader( 1, 256, CV_32S );
    cvSetData( lut_mat, lut, 0 );

    cvNamedWindow("image", 0);
    cvNamedWindow("histogram", 0);

	cvResizeWindow("image",640,680);


    cvCreateTrackbar("XOffset", "image", &_XOffset, 200, update_brightcont);
    cvCreateTrackbar("YOffset", "image", &_YOffset, 200, update_brightcont);
    cvCreateTrackbar("scale", "image", &_scale, 200, update_brightcont);
	cvCreateTrackbar("offset", "image", &_offset, 200, update_brightcont);


    update_brightcont(0);
//    cvSetMouseCallback( "image", on_mouse, 0 );
	numCldFile = 495;

    for(;;)
    {
        int c;
		if (numCldFile < 670 )
		{
			num = num + 1.0f;
			numCldFile = (int) floor(num);
		}
		else
		{
			numCldFile = 495;
			num =495.0f;
		}
		
		sprintf(file_name, ".\\GSNExample\\testN%d.bmp", numCldFile);
	//	cvReleaseImage(&src_image);

		src_image = cvLoadImage( file_name, 4 );


		if( !src_image )
		{
			printf("Image was not loaded.\n");
			return -1;
		}
		cvReleaseImage(&dst_image);

		dst_image = cvCloneImage(src_image);
		update_brightcont(0);
        c = cvWaitKey(0);
        switch( (char) c )
        {
        case '\x1b':
            printf("Exiting ...\n");
            goto exit_main;
        }
    }

exit_main:
    
    cvReleaseImage(&src_image);
    cvReleaseImage(&dst_image);
    cvReleaseImage(&color);
    cvReleaseImage(&edge);
    cvReleaseImage(&grey);

    cvReleaseHist(&hist);

    return 0;
}

#ifdef _EiC
main(1,"demhist.c");
#endif

